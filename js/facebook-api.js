// Create a request variable and assign a new XMLHttpRequest object to it.
// var request = new XMLHttpRequest()

var data = {
    companies: [
        {
            companyName: "Pedal Tour Fredericksburg",
            personName: "John Smith",
            currency: '$',
            comma: ',',
            point: '.',
            startDate: "2019-07-20",
            endDate: "current",
            adID: "746822885496646",
            pixelID: "505433253152121",
            accessToken: "EAAHUeZBdBRZCwBACm3dSle4FmYYojnFgLTfFOzSZA0GlbqjhwpuCajmrp2ZA9yBWMyT58obTcYKumvkw50SgzQJI6MZA0HEBxDFGZA6EbqxdFfoHdZAifdjfQDcqGLoIFipG4GDQHYBZBTvjTmgGqSpPXhYZBJOE6phiiFamLZBJG1AywZBnwDol2UXKoSdlOAZBmTUZD"
        },
        {
            companyName: "FunAmsterdam",
            personName: "John Smith",
            currency: '€',
            comma: ' ',
            point: ',',
            startDate: "2018-08-10",
            endDate: "current",
            adID: "1524582250940198",
            pixelID: "1940606546217579",
            accessToken: "EAAHUeZBdBRZCwBAB2WWkWGTmziOIFatd5wOVJZAdbvZAHppJrR6WTseWBBrZBN9LbhI7teCuodnecxg7UyuZC6ZCyRsChO94zGauN0sjPZAZB7Q5ZAr31IXlbd7krqa7TRjDyoSxsartRVhLpmK1G1Fi8axbNDQkZBUKVJk5W1TqLrmzLFwV0Y2FbQpJ7ZAyliiMkLgZD"
        },
        {
            companyName: "Party Bike San Jose",
            personName: "John Smith",
            currency: '$',
            comma: ',',
            point: '.',
            startDate: "2018-08-10",
            endDate: "current",
            adID: "496464261163879",
            pixelID: "2842642662431062",
            accessToken: "EAAHUeZBdBRZCwBAALZBPuNB2Do9RAG3u3YmSnrq7fqf5wgTI1wGrsdgT1XQIEhGWYtmOZBDZCBeB8qUOzc1beooixCORhgrN9HdmwNFvTnZCKqMT3z3IvAqQtLD9qicB1smoPvF91cmqleRhu6R8HIZAMT7FxiZAhG7vBT34HAQYz8iQJjNqMSYjdZAzBnndKKpoZD"
        },
        {
            companyName: "Trolley Pub Madison",
            personName: "John Smith",
            currency: '$',
            comma: ',',
            point: '.',
            startDate: "2019-03-01",
            endDate: "current",
            adID: "268549543895319",
            pixelID: "2134781140140462",
            accessToken: "EAAHUeZBdBRZCwBAFbwivTIkITLSkET5QFZAoOgaVdJAPRwzzrK0UwLKIXcwzLyO56ZAjOAjqJUvLgx4uXJ4kOgBoZBwqaXj6dk6Kn04PZAu0IRVAyJl0sGUBwww4Nij2UQVlFwszGIwzGdrtEGNy2PEkEjACh4JEqNDntxn96kQBpeFZAyRTuZBAZCy3Tocw9A9AZD"
        },
        {
            companyName: "Pet Wants",
            personName: "John Smith",
            currency: '$',
            comma: ',',
            point: '.',
            startDate: "2018-08-10",
            endDate: "current",
            adID: "101684286853402",
            pixelID: "362445347800162",
            accessToken: "EAAHUeZBdBRZCwBAJqGX1wfV72vN79B4sqiSZBWdMAL0dzq5mJDqGZBbDI9ZCecXuNl83SJFJYgK6KAVepL6SwxsPgobdxjExDXRdAZCWtyOAxcs8I9f3Cl5WyFI1uJsOtoBB5fsKZBFHWcYqqb7ZBNlqzWUtoVurPptEkT5urwmQmKs5BfVMWXeWc8vwYG1ZAfXoZD"
        },
        {
            companyName: "Sea Legs Pedal Pub",
            personName: "Thad Tremaine",
            currency: '$',
            comma: ',',
            point: '.',
            startDate: "2019-06-21",
            endDate: "current",
            adID: "549383325493786",
            pixelID: "609464836208069",
            accessToken: "EAAHUeZBdBRZCwBAKDr3u5EpiZCDWQpMV2FmsLZCZAXax75bntZA84l4goUqzyx1ZAgouRmxrxtuXjbYjfRzGytfAbJg0bQ8TyWZCdRov8VOZCtF2tM4kxLHemuHptW29LcGzxWOCY1bAoEeqcWnTuwYKTw7gmOJP4PUwA1M7Umpm4R50oZAMjqHWEdlloii5UeSg4ZD"
        }
    ]
}


var currentFirebaseUser = {}

firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser){
        console.log(firebaseUser)
	currentFirebaseUser = firebaseUser
	setupCompany(currentFirebaseUser)
    }
    else{
        console.log("not logged in")
        window.location = '..'
    }
})

setupCompany = function(user){

    console.log("getting old user object...")
    console.log(currentFirebaseUser)
    console.log("getting user...")
    console.log(user)
    var companyID = ""
    if(user[uid] == "UNuGnnBNoAbDokOIYzP7EeEeBr53"){
        companyID = "sealegspedalpub"
    }

    var companyRef = firebase.database().ref().child('companies')
    var myComapny = companyRef.child(companyID)
    var data = {
        companies:[
        ]
    }
    data[companies].push(myCompany)
}



var config = {
    fbEndpoint: "https://graph.facebook.com/",
    apiVersion: "v4.0/",
    adID: "665538237166461",
    accessToken: "EAAHUeZBdBRZCwBACjoEF53zVZCrZBC762cbWPWLkMJTZAKE2QbHJiT90GpRZCneUhbYEVxK4ZCXzeFNmg2FF0GeysarwM4LzNI0Uaanhm19RxnSpe1aVCrnEptecZBkBCeCeQDTiN5uqlJVTU5dZCynZCzBIUxMybpdcf9Fmi7FmiKXTd3Uxgm7fJrdjFGThPnl1YZD"
}

var insights_format = "act_746822885496646/insights?date_preset=lifetime&fields=spend"
var pixel_format = "505433253152121/stats?start_time=2018-08-10"

var companyIndex = 3
var date_preset_range = "lifetime"

var date_dict = {
    "lifetime": "All Time",
    "today": "Today",
    "past_7d": "Past Week",
    "past_30d": "Past Month"
}

commaify = function(number){
    isNeg = false
    number = parseFloat(number)
    if(number.toString().includes('.')){
        number = number.toFixed(2)
    }
    if(number < 0){
        isNeg = true
        number = number * -1
    }
    number = number.toString()

    var commas = Math.floor((number.length - 1) / 3)

    if(number.includes('.')){
        number = parseFloat(number).toFixed(2)

        for (i = 1; i < commas; i++){
            var index = number.length - 3 - (i * 3) - (i - 1)
            number = number.substring(0,index) + data['companies'][companyIndex]['comma'] + number.substring(index, number.length)
        }

        number = number.substring(0,number.length - 3) + data['companies'][companyIndex]['point'] + number.substring(number.length - 2, number.length)
    }
    else{
        for(i = 1; i <= commas; i++){
            var index = number.length - (i * 3) - (i - 1)
            number = number.substring(0,index) + data['companies'][companyIndex]['comma'] + number.substring(index, number.length)
        }
    }
    if(isNeg){
        return "-" + number
    }
    else{
        return number
    }
}

moneyify = function(number){
    number = commaify(parseFloat(number).toFixed(2))
    var currencySign = data['companies'][companyIndex]['currency']
    if(!number.includes(data['companies'][companyIndex]['point'])){
        number = number + data['companies'][companyIndex]['point'] + "00"
    }
    if(number.charAt(0) == "-"){
        return '-' + currencySign + number.substring(1, number.length)
    }
    else{
        return currencySign + number
    }
}
numberify = function(currency){
    var number = currency.split(data['companies'][companyIndex]['comma']).join('')
    number = nummber.split(data['companies'][companyIndex]['point']).join('.')
    var isNeg = false
    var n = 0
    if(number.charAt(0) == "-"){
        isNeg = true
        number = number.substring(1, number.length)
    }
    if(number.charAt(0) == data['companies'][companyIndex]['currency']){
        number = number.substring(1, number.length)
    }
    n = parseFloat(number)
    if(isNeg){
        n = n * -1
    }
    return n
}

getDateByRange = function(date_preset){
    var date = new Date()
    var sections = data['companies'][companyIndex]['startDate'].split('-')
    var year = sections[0]
    var month = sections[1] - 1
    var day = sections[2]
    var started = new Date(year, month, day, 00, 00, 00, 0)
    var date_7 = new Date()
    var date_30 = new Date()
    date_7.setDate(date_7.getDate() - 7)
    date_30.setDate(date_30.getDate() - 30)

    if(date_preset == "lifetime"){
        return data['companies'][companyIndex]['startDate']
    }
    else if(date_preset == "last_7d" && date_7 < started){
        return data['companies'][companyIndex]['startDate']
    }
    else if(date_preset == "last_30d" && date_30 < started){
        return data['companies'][companyIndex]['startDate']
    }
    else{
        var dd = date.getDate()
        if(date_preset == "last_7d"){
            date.setDate(date.getDate() - 7)
            dd = date.getDate()
        }
        else if(date_preset == "last_30d"){
            date.setDate(date.getDate() - 30)
            dd = date.getDate()
        }
        var mm = date.getMonth() + 1
        var yyyy = date.getFullYear()
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        }
        date = yyyy + '-' + mm + '-' + dd
        return date
    }
}

getTimerange = function(date_preset){
    var today = new Date
    var dd = today.getDate()
    var mm = today.getMonth() + 1
    var yyyy = today.getFullYear()
    var timerange = {
        since: "",
        until: ""
    }
    today = yyyy + '-' + mm + '-' + dd
    if(date_preset == "today"){
        var timerange = {
            since: today,
            until: today
        }
        return timerange
    }
    else if(date_preset == "lifetime"){
        var timerange = {
            since: data['companies'][companyIndex]['startDate'],
            until: today
        }
        return timerange
    }
    var date = new Date()
    var sections = data['companies'][companyIndex]['startDate'].split('-')
    var year = sections[0]
    var month = sections[1] - 1
    var day = sections[2]
    var started = new Date(year, month, day, 00, 00, 00, 0)

    var dateCheck = new Date()
    var offset = 0
    var d = ""
    var m = ""
    var y = ""
    if(date_preset == "last_7d"){
        offset = 7
        dateCheck.setDate(dateCheck.getDate() - 7)
        var d = dateCheck.getDate()
        var m = dateCheck.getMonth() + 1
        var y = dateCheck.getFullYear()
    }
    else if(date_preset == "last_30d"){
        offset = 30
        dateCheck.setDate(dateCheck.getDate() - 30)
        var d = dateCheck.getDate()
        var m = dateCheck.getMonth() + 1
        var y = dateCheck.getFullYear()
    }
    dateCheck.setDate(dateCheck.getDate() - offset)

    if(dateCheck < started){
        var timerange = {
            since: data['companies'][companyIndex]['startDate'],
            until: today
        }
        return timerange
    }
    dateCheck = y + '-' + m + '-' + d

    var timerange = {
        since: dateCheck,
        until: today
    }
    return timerange
}

// Get and update all dynamic naming properties
const nameLabel = document.getElementById('nameLabel')
const companyLabel = document.getElementById('companyLabel')
const companyTitle = document.getElementById('companyTitle')

// Get and update all statistical elements
const spentLabel = document.getElementById('spentLabel')
const madeLabel = document.getElementById('madeLabel')
const profitLabel = document.getElementById('profitLabel')

const purchasesLabel = document.getElementById('purchasesLabel')
const pageViewsLabel = document.getElementById('pageViewsLabel')
const startedCheckoutsLabel = document.getElementById('startedCheckoutsLabel')

const reachLabel = document.getElementById('reachLabel')
const impressionsLabel = document.getElementById('impressionsLabel')
const frequencyLabel = document.getElementById('frequencyLabel')

const commentsLabel = document.getElementById('commentsLabel')
const sharesLabel = document.getElementById('sharesLabel')
const likesLabel = document.getElementById('likesLabel')

// Make Request for Data!
// Getting pixel data
pixelRequest = function(companyIndex, date_preset = "lifetime"){
    var xhrAds = new XMLHttpRequest() // creating request object
    var requestURL = config['fbEndpoint'] + config['apiVersion'] + data['companies'][companyIndex]['pixelID'] + "/stats?aggregation=event_total_counts&start_time="
    var date = getDateByRange(date_preset)
    requestURL = requestURL + date
    requestURL = requestURL + "&access_token=" + data['companies'][companyIndex]['accessToken']
    xhrAds.open("GET", requestURL, true) // associates request attributes with XHR

    xhrAds.onload = function(e) { // when response received...
        response = JSON.parse(xhrAds.responseText)      
        console.log(response)
        var purchases = 0
        var views = 0
        var checkouts = 0
        var contacts = 0
        var subscribers = 0
        var r = response['data'][0]
        if(r){
            r = r['data']
        }
        for(type in r){
            var value = r[type]['value']
            var count = r[type]['count']
            if(value == "PageView"){
                views = count
            }
            else if(value == "Purchase"){
                purchases = count
            }
            else if(value == "InitiateCheckout"){
                checkouts = count
            }
            else if(value == "Contact"){
                contacts = count
            }
            else if(value == "Subscribe"){
                subscribers = count
            }
        }

        // TODO: Save Stats Locally in DB by quarter, month, etc

        // Update Purchases, Page Views, and Started Checkouts
        purchasesLabel.innerText = commaify(purchases)
        pageViewsLabel.innerText = commaify(views)
        startedCheckoutsLabel.innerText = commaify(checkouts)
    }

    xhrAds.onerror = function(e){ // when error response received...
        console.error(xhrAds.statusText)  
    }

    xhrAds.send(null) // send request
}

// Getting insights data
insightsRequest = function(companyIndex, date_preset = "lifetime"){
    var xhrAds = new XMLHttpRequest() // creating request object
    var requestURL = config['fbEndpoint'] + config['apiVersion'] + "act_" + data['companies'][companyIndex]['adID'] + "/insights?"
    requestURL = requestURL + "time_range=" + JSON.stringify(getTimerange(date_preset))
    requestURL = requestURL + "&fields=impressions,reach,spend,actions,action_values" + "&access_token=" + config['accessToken']
    xhrAds.open("GET", requestURL, true) // associates request attributes with XHR

    xhrAds.onload = function(e) { // when response received...
        response = JSON.parse(xhrAds.responseText)
        console.log(response)

        var spend = 0
        var reach = 0
        var impressions = 0
        var actions = 0

        var shares = 0
        var comments = 0
        var likes = 0
        var revenue = 0
	console.log(response)
        var r = response['data'][0]
        if(r){
            if('spend' in r){
                if(r['spend'].includes('.')){
                    spend = r['spend'].toString() + "00"
                }
                else{
                    spend = r['spend'].toString() + ".00"
                }
            }
            if('reach' in r){
                reach = r['reach']
            }
            if('impressions' in r){
                impressions = r['impressions']
            }
            if('actions' in r){
                actions = r['actions']
            }
            for(a in actions){
                if(actions[a]['action_type'] == "post"){
                    shares = actions[a]['value']
                }
                else if(actions[a]['action_type'] == "comment"){
                    comments = actions[a]['value']
                }
                else if(actions[a]['action_type'] == "like"){
                    likes = actions[a]['value']
                }
            }
            var action_values = {}
            if(response['data'][0]['action_values']){
                action_values = response['data'][0]['action_values']
            }
            for(a in action_values){
                if(action_values[a]['action_type'] == "offsite_conversion.fb_pixel_purchase"){
                    revenue = action_values[a]['value']
                }
            }
        }

        // TODO: ADD CODE FOR Purchases, Page Views, and Started Checkouts
        var adSpend = moneyify(spend)
        var adRevenue = moneyify(revenue)
        var adProfit = moneyify(revenue - spend)
        var adFrequency = 0
        if(reach != 0){
            adFrequency = (impressions/reach).toFixed(4).toString()
            frequencyLabel.innerText = adFrequency.substring(0,adFrequency.length - 5) + data['companies'][companyIndex]['point'] + adFrequency.substring(adFrequency.length - 4, adFrequency.length)
        }
        else{
            frequencyLabel.innerText = 0
        }

        profitLabel.innerText = adProfit
        spentLabel.innerText = adSpend
        madeLabel.innerText = adRevenue

        reachLabel.innerText = commaify(reach)
        impressionsLabel.innerText = commaify(impressions)

        commentsLabel.innerText = commaify(comments)
        sharesLabel.innerText = commaify(shares)
        likesLabel.innerText = commaify(likes)
    }

    xhrAds.onerror = function(e){ // when error response received...
        console.error(xhrAds.statusText)  
    }

    xhrAds.send(null) // send request
}


// Time Range Dropdown Buttons and Clicks
const timerangeToday = document.getElementById('timerange-today')
const timerangeWeek = document.getElementById('timerange-week')
const timerangeMonth = document.getElementById('timerange-month')
const timerangeAll = document.getElementById('timerange-all')
const timerangeDropdown = document.getElementById('timerangeDropdown')

timerangeToday.addEventListener('click', e => {
    date_preset_range = "today"
    webpageLoad(companyIndex, "today")
    timerangeDropdown.innerText = "Today"
})
timerangeWeek.addEventListener('click', e => {
    date_preset_range = "last_7d"
    webpageLoad(companyIndex, "last_7d")
    timerangeDropdown.innerText = "Past Week"
})
timerangeMonth.addEventListener('click', e => {
    date_preset_range = "last_30d"
    webpageLoad(companyIndex, "last_30d")
    timerangeDropdown.innerText = "Past Month"
})
timerangeAll.addEventListener('click', e => {
    date_preset_range = "lifetime"
    webpageLoad(companyIndex, "lifetime")
    timerangeDropdown.innerText = "All Time"
})

// Comapny Dropdown Buttons and Clicks
const company_0 = document.getElementById('company-0')
const company_1 = document.getElementById('company-1')
const company_2 = document.getElementById('company-2')
const company_3 = document.getElementById('company-3')
const company_4 = document.getElementById('company-4')
const company_5 = document.getElementById('company-5')
const companyDropdown = document.getElementById('companyDropdown')

company_0.addEventListener('click', e => {
    companyIndex = 0
    webpageLoad(companyIndex, date_preset_range)
    companyDropdown.innerText = data['companies'][companyIndex]['companyName']
})
company_1.addEventListener('click', e => {
    companyIndex = 1
    webpageLoad(companyIndex, date_preset_range)
    companyDropdown.innerText = data['companies'][companyIndex]['companyName']
})
company_2.addEventListener('click', e => {
    companyIndex = 2
    webpageLoad(companyIndex, date_preset_range)
    companyDropdown.innerText = data['companies'][companyIndex]['companyName']
})
company_3.addEventListener('click', e => {
    companyIndex = 3
    webpageLoad(companyIndex, date_preset_range)
    companyDropdown.innerText = data['companies'][companyIndex]['companyName']
})
company_4.addEventListener('click', e => {
    companyIndex = 4
    webpageLoad(companyIndex, date_preset_range)
    companyDropdown.innerText = data['companies'][companyIndex]['companyName']
})
company_5.addEventListener('click', e => {
    companyIndex = 5
    webpageLoad(companyIndex, date_preset_range)
    companyDropdown.innerText = data['companies'][companyIndex]['companyName']
})



// Actually Loading the Webpages
setupDashboard = function(companyIndex, date_preset_range){
    nameLabel.innerText = data['companies'][companyIndex]['personName']
    companyLabel.innerText = data['companies'][companyIndex]['companyName']
    companyTitle.innerText = data['companies'][companyIndex]['companyName']
    company_0.innerText = data['companies'][0]['companyName']
    company_1.innerText = data['companies'][1]['companyName']
    company_2.innerText = data['companies'][2]['companyName']
    company_3.innerText = data['companies'][3]['companyName']
    company_4.innerText = data['companies'][4]['companyName']
    company_5.innerText = data['companies'][5]['companyName']
}

setupSettings = function(companyIndex){
    // Add Company Attributes to Settings Tab
    const companyNameInput = document.getElementById('companyNameInput')
    const personNameInput = document.getElementById('personNameInput')
    companyNameInput.value = data['companies'][companyIndex]['companyName']
    personNameInput.value = data['companies'][companyIndex]['personName']
}

webpageLoad = function(companyIndex, date_preset_range){
    insightsRequest(companyIndex, date_preset_range)
    pixelRequest(companyIndex, date_preset_range)
    setupDashboard(companyIndex, date_preset_range)
    setupSettings(companyIndex)
}
webpageLoad(companyIndex)
